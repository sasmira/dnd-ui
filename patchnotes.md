# Patchnotes
## Compatibility
- Ready for v11 version.

## v11.311.1
- Fix Scene list variant (GM/Player)

## v11.306.3
- fix the problem of the interface shifting upwards.

## v11.306.2
- fix the problem overrides camera bar buttoms

## v11.306.1
- First release for Foundry VTT v11

## v10.284.1
- First release for Foundry VTT v10

## v1.0.0
- Initial release 
